# Guia para UICollectionView y UITableView cell´s

Primero que nada debemos crear dentro del proyecto la carpeta UIViews/cells/collectionCells para las celdas de los UICollectionView y la carpeta UIViews/cells/tableCells para las celdas de los UITableView

![UNO](readme-1.png)

## Celdas simples 

Las celdas simples constan de una sola clase o archivo .swift
``` swift
import UIKit
class ItemTableViewCell : UITableViewCell {
    @IBOutlet var itemImageView:UIImageView?
    @IBOutlet var itemNameLabel:UILabel?
    
    /*Puede ser por parametros separados*/
    func configure(itemName:String, itemImage:UIImage){
        self.itemNameLabel?.text = itemName

        /* aqui regularmente se usa alguna 
            libreria como kingfisher para cargar 
            las imagenes de una url */
        self.itemImageView?.image = itemImage
    }
    /*O por algun modelo en especifico*/
    func configure(item:Item){
        self.itemNameLabel?.text = item.name

        /* aqui regularmente se usa alguna 
            libreria como kingfisher para cargar 
            las imagenes de una url */
        self.itemImageView?.image = item.image
    }
}
```

En el codigo anterior tenemos el ejemplo de una clase que hereda de UITableViewCell (podria ser de alguna otra subclase o bien UICollectionViewCell en caso de las celdas para las UICollectionView).

Como se puede observar la clase necesita tener los IBOutlet necesarios para definir sus subvistas y una funcion "configure" que nos sirva para rellenar o configurar dichas subvistas.

#### Configurando la Celda Simple en el Storyboard

1.- Se tiene que agregar un elemento UITableViewCell o UICollectionViewCell dentro del elemento UITableView o UICollectionView dependiendo de lo que se necesite

![UNO](readme-2.png)

2.- Asignar la clase Custom anteriormente creada al elemento

 ![UNO](readme-3.png)
 
 3.- Asignar un identificador de la celda
 
 ![UNO](readme-4.png)
 
 4.- Agregar los elementos visuales declarados en la clase swift y ligarlos a dicha clase
 
 ![UNO](readme-5.gif)
 
 5.- Usar la celda en el UITableViewDataSource o UICollectionViewDataSource
 
``` swift
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell:ItemTableViewCell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath) as! ItemTableViewCell
            cell.configure(itemName: "Name", itemImage: UIImage())
            return cell
        }
```

## Celdas + .xib

En lo anterior hemos visto como definimos todos los aspectos visuales de una celda dentro de los Storyboards de un UIViewController, esto puede llegar a ser util si queremos tener la posibilidad de que una misma clase de celda pueda verse diferente dependiendo del UIViewController en que se utilize o incluso en el mismo UIViewController dependiendo de su identificador. 

Pero si lo que buscamos es que las celdas se vean exactamente igual independientemente de donde se utilicen tendriamos que agregarles su archivo '.xib'.

El archivo xib como regla interna lo llamaremos exactamente igual que el nombre de la clase.

![UNO](readme-6.png)

Vamos a hacer un ejemplo de una custom CollectionViewCell con xib

1.- Archivo ItemCollectionViewCell.swift

``` swift
import UIKit
class ItemCollectionViewCell : UICollectionViewCell {
    @IBOutlet var itemImageView:UIImageView?
    @IBOutlet var itemNameLabel:UILabel?
    
    /*Puede ser por parametros separados*/
    func configure(itemName:String, itemImage:UIImage){
         self.itemNameLabel?.text = itemName
    
          /* aqui regularmente se usa alguna 
                libreria como kingfisher para cargar 
                las imagenes de una url */
          self.itemImageView?.image = itemImage
    }

    /*O por algun modelo en especifico*/
    func configure(item:Item){
        self.itemNameLabel?.text = item.name
    
        /* aqui regularmente se usa alguna 
                libreria como kingfisher para cargar 
                las imagenes de una url */
        self.itemImageView?.image = item.image
    }
}
```

2.- Crear el archivo ItemCollectionViewCell.xib

![UNO](readme-7.png)

3.- Agregar un elemento UICollectionViewCell

![UNO](readme-8.gif)

4.- Agregar la clase ItemCollectionViewCell al elemento

![UNO](readme-9.png)

5.- Agregar los elementos visuales declarados en la clase swift y ligarlos a dicha clase

![UNO](readme-10.gif)

6.- En el codigo de tu UIViewController registrar la celda en el UICollectionView

``` swift
    @IBOutlet var collectionView:UICollectionView?

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.register(UINib(nibName: "ItemCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "mycell")
    }
```

7.- Usar la celda en el UITableViewDataSource o UICollectionViewDataSource

``` swift
func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell:ItemCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "mycell", for: indexPath) as! ItemCollectionViewCell
        cell.configure(itemName: "Hola ", itemImage: UIImage())
        return cell
    }
```


## Nota

Se busca ubicar todas las celdas en esas dos carpetas para poder ubicar facilmente todos los tipos de celda existentes en el proyecto y poder reutilizarlas independientemente de su caso de uso o vista. 








 
 
 
 




