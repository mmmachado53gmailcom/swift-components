//
//  TabCustom1.swift
//  TestComponents
//
//  Created by Miguel Machado on 9/18/19.
//  Copyright © 2019 Miguel Machado. All rights reserved.
//

import UIKit
class TabCustom1: Tab {

    /*Agrego las opciones para manipular los colores desde los storyboards*/
    @IBInspectable var colorSelected:UIColor = UIColor.black {
        didSet{
            self.updateColors()
        }
    }

    @IBInspectable var colorUnselected:UIColor = UIColor.blue {
        didSet{
            self.updateColors()
        }
    }
    /*Agrego las opciones para manipular los paddings*/
    @IBInspectable var paddingTop: CGFloat = 0
    @IBInspectable var paddingLeft: CGFloat = 0
    @IBInspectable var paddingRight: CGFloat = 0
    @IBInspectable var paddingBottom: CGFloat = 0

    override var contentEdgeInsets: UIEdgeInsets {
        set{}
        get{
            return UIEdgeInsets(top: titleEdgeInsets.top + paddingTop, left: titleEdgeInsets.left + paddingLeft, bottom: titleEdgeInsets.bottom + paddingBottom, right: titleEdgeInsets.right + paddingRight)
        }
    }


    /*Agrego bordes*/
    override func style() {
        super.style()
        self.layer.borderWidth = 1
        self.layer.cornerRadius = 5
    }

    /*Hago que los bordes se coloreen segun el estado seleccionado/no seleccionado*/
    override func setSelectedStyle() {
        self.layer.borderColor = colorSelected.cgColor
        self.setTitleColor(colorSelected, for: .normal)
    }

    override func setUnselectedStyle() {
        self.layer.borderColor = colorUnselected.cgColor
        self.setTitleColor(colorUnselected, for: .normal)
    }


    func updateColors(){
        if select==true {
            setSelectedStyle()
        }else{
            setUnselectedStyle()
        }
    }
}