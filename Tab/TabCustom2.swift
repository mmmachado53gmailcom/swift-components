//
//  TabCustom2.swift
//  TestComponents
//
//  Created by Miguel Machado on 9/18/19.
//  Copyright © 2019 Miguel Machado. All rights reserved.
//

import UIKit
class TabCustom2: TabCustom1 {

    /*Agrego una barra indicadora*/
    private let indicatorView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 3))

    /*Agrego la barra indicadora dentro de la vista*/
    override func style() {
        self.backgroundColor = UIColor.white.withAlphaComponent(0.0)
        self.indicatorView.backgroundColor = self.colorSelected
        self.addSubview(indicatorView)

    }

    /*La función draw se ejecuta cada que se reajusta el tamaño de la vista
        por lo tanto también se reajusta el tamaño de la barra indicadora
     */
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        /*reposition indicator view*/
        let x:CGFloat = 0
        let y:CGFloat = self.frame.height - self.indicatorView.frame.height
        let w:CGFloat = self.frame.width
        let h:CGFloat = self.indicatorView.frame.height
        self.indicatorView.frame = CGRect(x: x, y: y, width: w, height: h)
    }

    /*Manipulo la visibilidad de la barra indicadora y los colores según el estado seleccionado / No seleccionado*/
    override func setSelectedStyle(){
        self.indicatorView.alpha = 1.0
        self.setTitleColor(self.colorSelected, for: .normal)
        self.indicatorView.backgroundColor = self.colorSelected
    }
    override func setUnselectedStyle(){
        self.indicatorView.alpha = 0
        self.setTitleColor(self.colorUnselected, for: .normal)
    }
}
