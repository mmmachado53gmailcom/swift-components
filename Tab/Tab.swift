//
//  Tab.swift
//  TestComponents
//
//  Created by Miguel Machado on 9/18/19.
//  Copyright © 2019 Miguel Machado. All rights reserved.
//

import UIKit


@IBDesignable class Tab: UIButton {

    @IBOutlet var delegate:TabButtonDelegate?

    var parentButton:Tab?
    @IBOutlet var otherTabs:[Tab] = []{
        didSet{
            self.bindAllOtherTabs()
        }
    }

    @IBInspectable var select:Bool = false {
        didSet{
            if select==true {
                setSelectedStyle()
            }else{
                setUnselectedStyle()
            }
        }
    }


    @IBInspectable var identifier:String = ""


    private let selectedColor:UIColor = UIColor.blue
    private let unselectedColor:UIColor = UIColor.black

    override init(frame: CGRect) {
        super.init(frame: frame)
        style()
        if select==true {
            setSelectedStyle()
        }else{
            setUnselectedStyle()
        }
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        style()
        if select==true {
            setSelectedStyle()
        }else{
            setUnselectedStyle()
        }
    }

    func style() {
        self.backgroundColor = UIColor.white.withAlphaComponent(0.0)
    }



    func setSelectedStyle(){
        self.setTitleColor(selectedColor, for: .normal)
    }
    func setUnselectedStyle(){
        self.setTitleColor(unselectedColor, for: .normal)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if(isTouchInside){
            self.touchUpInside()
        }
        super.touchesEnded(touches, with: event)
    }

    private func touchUpInside(){
        self.setSelect(select: true)
        if let d:TabButtonDelegate = self.parentButton?.delegate{
            d.tabButtonDelegate(selectedTabIdentifier: self.identifier)
        }else{
            self.delegate?.tabButtonDelegate(selectedTabIdentifier: self.identifier)
        }
    }

    public func setSelect(select:Bool){
        if select == false {
            self.select = select
            return
        }
        var allTabs:[Tab] = []
        parentButton?.select = false
        if let otherT:[Tab] = parentButton?.otherTabs  {
            allTabs = otherT
        }else{
            allTabs = self.otherTabs
        }

        for tab in allTabs {
            tab.select = false
        }

        self.select = true
    }

    private func bindAllOtherTabs(){
        for b in self.otherTabs{
            b.parentButton = self
        }
    }

}


@objc protocol TabButtonDelegate {
    func tabButtonDelegate(selectedTabIdentifier:String)
}