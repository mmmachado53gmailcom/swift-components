# Tab Class

![preview](readme-1.png)

### Descripción
Es una simple sub clase de UIButton. Agrega un estado activo/inactivo y una forma de ligar facilmente muchos elementos entre si permitiendo que interactuen como lo harian los input "radio" en html, se puede perzonalizar para que sirva con un control de tabs, radio buttons, switches. etc.

### Instalación
1.- Agrega el archivo "Tab.swift" a tu proyecto

2.- (Opcional) Agregar archivos TabCustom1.swift (sub clase de Tab estilizado con bordes y con atributos "inspectables") y TabCustom2.swift (Sub clase de TabCustom1 estilizado con un indicador inferior)

### Agregando a un Storyboard
1.- Agrega un elemento de tipo UIButton a una vista

2.- Asigna la clase "Tab" (o TabCustom1, TabCustom2) al elemento agregado

![step2](readme-2.png)

3.- Agrega otros elementos iguales a la vista

4.- Agrega los outlets para los otros elementos

![step4](readme-4.gif)

### Escuchando sus cambios de estado
1.- Agrega un "identificador" por cada tab (tambien puedes agregar su estado inicial que por default es deseleccionado)

![step2](readme-5.png)

2.- Implementar TabButtonDelegate en el UIViewController

```swift
extension ViewController : TabButtonDelegate{
    func tabButtonDelegate(selectedTabIdentifier: String) {
        print(selectedTabIdentifier)
    }
}
```

3.- Asignar el delegate en uno de los tabs (también se puede asignar el delegate en el codigo del view controller "tabInstance.delegate = self"")
 
 ![step4](readme-6.gif)
 
 
### Metodo setSelect

Se puede usár el metodo "setSelect(select:Bool)"  de la clase Tab para seleccionar de manera programada especificamente un elemento, si el parametro "select" va en true esto cambiaria el estado de seleccion a selected en el elemento y en unselected al resto de los elementos ligados. NOTA este metodo no dispara las acciones del delegate

 
### Heredar Tab para perzonalizar

Basicamente se necesitan sobreescribir 3 funciones (puedes revisar, los archivos TabCustom1.swift y TabCustom2.swift)

1.- style.- se ejecuta al inicio es para setear el estilo general del tab. 
```swift
override func style() {
            
        }
```

2.- setSelectedStyle.- se ejecuta cuando el estado es seleccionado, aquí puedes estilizar el tab al momento de estár seleccionado.
```swift
override func setSelectedStyle() {
            
        }
```

3.- setUnselectedStyle.- se ejecuta cuando el estado es deseleccionado, aquí puedes estilizar el tab al momento de estár deseleccionado.
```swift
override func setUnselectedStyle() {
            
        }
```



 